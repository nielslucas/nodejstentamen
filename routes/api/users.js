var express = require('express');
var router = express.Router();
User = require('../../models/users');
Game = require('../../models/games');

/* Get users. */
router.get('/', function(response, res, next) {
    User.getUsers(function (error, users) {
        if(error) {
            throw error;
        } else {
            res.json(users)
        }
    });
});

/* Create a new user*/
router.post('/', function(response, res, next) {

    var user = response.body;

    User.addUser(user, function (error, user) {
        if(error) {
            throw error;
        } else {
            res.json(user)
        }
    });
});

/* Get user  by id */
router.get('/:id', function(response, res, next) {
    User.getUserById(response.params.id, function (error, user) {
        if(error) {
            throw error;
        } else {
            res.json(user)
        }
    });
});

/* update a user*/
router.put('/:id', function(response, res, next) {
    var id = response.params.id;
    var user = response.body;

    User.updateUser(id, user, { new: true },function (error, userResult) {
        if(error) {
            throw error;
        } else {
            res.json(userResult)
        }
    });
});

/* remove a user*/
router.delete('/:id', function(response, res, next) {
    var id = response.params.id;

    User.deleteUser(id, function (error, user) {
        if(error) {
            throw error;
        } else {
            res.json(user)
        }
    });
});

/* Add game to user */
router.post('/:userId/game/:gameId', function(req, res, next) {

    var userId = req.params.userId;
    var gameId = req.params.gameId;

    Game.getGameById(gameId, function (error, game) {
        if(error)
            throw error;

        User.getUserById(userId, function (error, user) {
            if(error)
                throw error;

            //res.json(game._id);

            var test = game._id;

            user.games.push(game._id);

            User.updateUser(userId, user, { new: true }, function (error, user) {
                if(error) {
                    throw error;
                } else {
                    res.json(user);
                }
            });
        });
    });
});

module.exports = router;
