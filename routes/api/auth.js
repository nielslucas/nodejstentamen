var express = require('express');
var router = express.Router();
User = require('../../models/users');
Token = require('../../models/tokens');
var randomString = require("randomstring");

/* Get users. */
router.post('/login', function(req, res, next) {

    User.getUserByLogin(req.body, function (error, user) {
        if(error) {
            throw error;
        } else {

            var token = {
                token: randomString.generate(),
                user: user[0]._id
            };

            Token.saveToken(token, function (error, token ) {
                if(error) {
                    throw error;
                } else {
                    res.json(token);
                }
            });
        }
    });
});

router.post('/checkToken', function(req, res, next) {
    Token.authorization(req.body.token, function (error, tokens) {
        if(error) {
            throw error;
        } else {
            res.json(tokens)
        }
    });
});

router.get('/tokens', function(req, res, next) {
    Token.getTokens(function (error, tokens) {
        if(error) {
            throw error;
        } else {
            res.json(tokens)
        }

    });
});

module.exports = router;
