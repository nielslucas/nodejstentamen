var express = require('express');
var router = express.Router();
Game = require('../../models/games');

/* Get users. */
router.get('/', function(req, res, next) {
    User.getUsers(function (error, users) {
        if(error) {
            throw error;
        } else {
            res.json(users)
        }
    });
});

/* Create a new user*/
router.post('/', function(req, res, next) {

    var game = req.body;

    Game.addGame(game, function (error, user) {
        if(error) {
            throw error;
        } else {
            res.json(user)
        }
    });
});

module.exports = router;
