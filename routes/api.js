var express = require('express');
var router = express.Router();

var users = require('./api/users');
var auth = require('./api/auth');
var games = require('./api/games');

/* GET home page. */
router.use('/users', users);
router.use('/auth', auth);
router.use('/games', games);

module.exports = router;
