var mongoose = require('mongoose');
Game = require("./games");

// Users Schema
var tokenObject = mongoose.Schema({
    token: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    user2: {
        type: String,
        default: "test"
    },
    created: {
        type: Date,
        default: Date.now()
    }
});

var Token = module.exports = mongoose.model('Tokens', tokenObject);

// Insert new token
module.exports.saveToken = function (token, callback) {
    Token.create(token, callback);
};

module.exports.getTokens = function (callback, limit) {
    Token.find(callback).limit(limit);
};
module.exports.authorization = function (token, callback) {
    Token.find({"token": token}, callback).limit(1);
};

