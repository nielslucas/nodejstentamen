var mongoose = require('mongoose');
Game = require("./games");

// Users Schema
var userObject = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    games: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Games'
    }],
    created: {
        type: Date,
        default: Date.now()
    }
});

var User = module.exports = mongoose.model('Users', userObject);

// Get Users
module.exports.getUsers = function (callback, limit) {
    User.find(callback).limit(limit);
};

// Get user by id
module.exports.getUserById = function (userId, callback) {
    User.findById(userId, callback);
};

// Add user
module.exports.addUser = function (user, callback) {
    User.create(user, callback);
};

module.exports.updateUser = function (id, user, options, callback) {
  var query = {_id: id};
  var update = {
      name: user.name,
      games: user.games
  };
  User.findOneAndUpdate(query, update, options, callback)
};

module.exports.deleteUser = function (id, callback) {
  var query = {
      _id: id
  };
  User.remove(query, callback);
};

// Auth login

// Get User by email and password
module.exports.getUserByLogin = function (params, callback) {
    var query = {
        email: params.email,
        password: params.password
    };

    User.find(query, callback).limit(1);
};

// User Games
module.exports.addGameToUser = function (userId, game, callback) {
    User.findById(userId, callback);
};
