var mongoose = require('mongoose');

// Game Schema
var gameObject = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now()
    }
});

var Game = module.exports = mongoose.model('Games', gameObject);

// Get user by id
module.exports.getGameById = function (gameId, callback) {
    Game.findById(gameId, callback);
};

// Add user
module.exports.addGame = function (game, callback) {
    Game.create(game, callback);
};